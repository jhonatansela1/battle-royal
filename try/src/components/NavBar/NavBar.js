import Styles from './NavBar.module.css'
import { NavLink } from 'react-router-dom'

const NavBar = () => {
    return (
        <nav className={Styles.navWrapper}>
            <ul className={Styles.navList}>
                {/* home is to show all the users */}
                <li><NavLink to='/home' className={Styles.link}>Home</NavLink></li> 
                <li><NavLink to='/allAlbums' className={Styles.link}>All Albums</NavLink></li> 
                
            </ul>
        </nav>
    );
}

export default NavBar