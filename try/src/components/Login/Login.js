import Styles from './Login.module.css'
import logo from '../../images/logo.jpg'
import { useState } from 'react'
import { Navigate } from 'react-router-dom'
import { useFetchUser } from "../../hooks/fetchUser";

const Login = ({userInput , setUserInput, setUserserInputButton}) => {

    const [usersList, setUsersList] = useState([])
  
    
    const [isValid, setIsValid] = useState(' ')
    
    const [passInput, setPassInput] = useState('')
    
    useFetchUser(setUsersList)
    const handleSignIn = () => {
        // usersList[0].password
        if (userInput.length !== 0 && passInput.length !== 0){ 
            try{
                usersList.forEach(element => {
                    console.log(`checking : ${element.username} == ${userInput}`)
                    if(userInput == element.username){
                        if(passInput == element.password)
                        {
                            
                            setIsValid('')
                            setUserserInputButton(userInput)
                            throw 'Break';
                        }
                        else{
                            
                            setIsValid("wrong password")
                        }
                    }
                    else{
                        
                        setIsValid(`Username ${userInput} does not exist`)
                    }
                    
                });
            }
            catch (e) {
                if (e !== 'Break') throw e
              }

        }
        else{
            setIsValid('Please Enter Username And Password!')
        }
    }


    const handleUserChange = (value) => {
        setUserInput(value)
        
    }
    const handlePassChange = (value) => {
        setPassInput(value)
        
    }
    
    return (
        <div className={Styles.loginWrapper}>
            <img src={logo}/>
            <input placeholder='Username' onChange={(event) => handleUserChange(event.target.value)}/>
            <input placeholder='Password' type='password' onChange={(event) => handlePassChange(event.target.value)}/>
            <button onClick={handleSignIn} className={Styles.signin}>Sign In</button>
            {isValid.length === 0 ? <Navigate to='/home'/> : <span className={Styles.errorMess}>{isValid}</span>}
        </div>
    )
}

export default Login