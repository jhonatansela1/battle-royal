import axios from "axios";
import { useEffect } from "react";



export const useFetchUser = async (setUsersList) => {
    useEffect(() => {
        async function getUsers(){
            try{
                const {data} = await axios.get(`http://52.224.2.235:4000/users`)
                setUsersList(data)
            }
            catch(err){
                console.error(err)
            }
        }
        getUsers()
    }, [])
}