import NavBar from './components/NavBar';
import Home from './components/Home';

import { BrowserRouter as Router, Routes as Switch, Route} from 'react-router-dom' 
import Styles from './App.module.css'
import {useState} from 'react'

import Login from './components/Login';
import Admin from './components/Admin';


function App() {

  const [userInput, setUserInput] = useState('')
  const [userInputButton, setUserserInputButton] = useState('')
  

  const userPagesRoutes = []
  

  return (
    <Router>
      <div >
        <div className={Styles.headWrapper}>
          <h1>CYTAK<span>OM</span></h1>
          {userInputButton.length !== 0 && <h1>Hello {userInputButton}</h1>}
          {/* <NavBar /> */}
        </div>
        <div className={Styles.pageWrapper}>
          <Switch>
              <Route path='/' element= {<Login userInput={userInput} setUserInput={setUserInput} setUserserInputButton={setUserserInputButton}/>}/>
              <Route path='/home' element= {<Home/>}/>
              <Route path='/admin' element= {<Admin/>}/>
              {userPagesRoutes}
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
